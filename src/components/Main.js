import React from 'react';
import { NavLink, Route, Switch } from 'react-router-dom';

// styles
import './styles/Main.scss';

// components
import Header from '../ui_components/header';
import Nav from '../ui_components/nav';
import Rules from './Rules';
import About from './About';

export default function Main({ match }) {
  const _renderMain = () => (
    <>
      <h1>Добро пожаловать на главную страницу нашей игры!</h1>
      <p>
        Перед началом игры вы можете{' '}
        <NavLink to="/home/rules">ознакомиться с правилами</NavLink> или же
        сразу <NavLink to="/game">начать новую игру.</NavLink>
      </p>
    </>
  );

  const { isExact } = match;

  return (
    <>
      <Header>
        <h1>Поиск совпадений</h1>
        <Nav>
          {isExact && <NavLink to="/game">Начать игру</NavLink>}
          {!isExact && <NavLink to="/">Вернуться на главную</NavLink>}
          <NavLink to="/home/rules">Правила</NavLink>
          <NavLink to="/home/about">Об игре</NavLink>
        </Nav>
      </Header>
      <hr />

      <Switch>
        <Route path="/home/" exact render={_renderMain} />
        <Route path="/home/rules" component={Rules} />
        <Route path="/home/about" component={About} />
      </Switch>
    </>
  );
}
