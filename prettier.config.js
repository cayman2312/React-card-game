module.exports = {
  tabWidth: 2,
  printWidth: 80,
  semi: true,
  singleQuote: true,
  jsxBracketSameLine: false,
  bracketSpacing: true,
  trailingComma: 'all',
};
