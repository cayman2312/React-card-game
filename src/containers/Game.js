import React, { Component } from 'react';

// styles
import './Game.scss';

// components
import Header from '../ui_components/header';
import Nav from '../ui_components/nav';
import Cards from '../components/Cards/Cards';
import Popup from '../ui_components/popup';
import Settings from './Settings';

export default class Game extends Component {
  constructor(props) {
    super(props);
    this.timer = null;
    this.availableCompare = false;

    this.state = {
      gameStatus: 'setSettings', // running, win, lose
      timeLeft: null,
      cards: [],
      checkedCards: [],
      firstCard: null,
      secondCard: null,
    };
  }

  componentDidUpdate() {
    const { gameStatus } = this.state;

    if (gameStatus === 'win') {
      clearInterval(this.timer);
    }
  }

  startTimer = (delay = 0) => {
    this.setGameStatus('running');

    setTimeout(() => {
      this.availableCompare = true;

      const tick = () => {
        if (this.state.gameStatus === 'lose') return;

        this.timer = setTimeout(() => {
          this.setState((prevState) => ({
            timeLeft: prevState.timeLeft - 1,
            gameStatus: prevState.timeLeft === 1 ? 'lose' : 'running',
          }));
          tick();
        }, 1000);
      };

      tick();
    }, delay * 30);
  };

  setGameStatus = (gameStatus) => this.setState({ gameStatus });

  setTimeLeft = (number, difficulty) => {
    let timeLeft;
    switch (difficulty) {
      case 1:
        timeLeft = number * 4;
        break;
      case 2:
        timeLeft = number * 3;
        break;
      case 3:
        timeLeft = Math.round(number * 2.5);
        break;
      default:
        break;
    }

    this.setState({ timeLeft });
  };

  setProperties = ({ cardAmount, difficulty, isRepeat }) => {
    this.setTimeLeft(cardAmount, difficulty);
    this.getCards(cardAmount, isRepeat);
  };

  getCards = (cardAmount, isRepeat) => {
    let arr;
    if (isRepeat) {
      arr = new Array(+cardAmount / 2)
        .fill('')
        .map(() => Math.round(Math.random() * 19) + 1);
    } else {
      let set = new Set();
      while (set.size < +cardAmount / 2) {
        set.add(Math.round(Math.random() * 19) + 1);
      }
      arr = [...set];
    }

    // получаем итоговый массив карт, клонируем и перемешиваем
    const cards = arr
      .concat(arr)
      .sort(() => Math.random() - 0.5)
      .map((card, index) => ({
        id: index,
        img: require(`../assets/images/${card}.jpg`),
      }));

    this.setState({ cards });
    this.startTimer(cards.length);
  };

  compareCards = (card, active) => {
    if (!this.availableCompare) return;

    if (active) return this.setState({ firstCard: null });

    const { firstCard } = this.state;

    if (!firstCard) return this.setState({ firstCard: card });

    this.setState({ secondCard: card });

    this.availableCompare = false;
    setTimeout(() => (this.availableCompare = true), 600);

    setTimeout(() => {
      if (firstCard.img === card.img) {
        // добавляем карты в угаданные
        this.setState((prevState) => ({
          checkedCards: [...prevState.checkedCards, firstCard.id, card.id],
          firstCard: null,
          secondCard: null,
        }));

        // проверяем победу
        setTimeout(() => {
          const { cards, checkedCards } = this.state;
          if (cards.length === checkedCards.length) {
            this.setGameStatus('win');
          }
        }, 300);
      } else {
        setTimeout(
          () => this.setState({ firstCard: null, secondCard: null }),
          300,
        );
      }
    }, 300);
  };

  restartGame = (e) => {
    e.preventDefault();

    clearInterval(this.timer);
    this.setState({
      cards: [],
      checkedCards: [],
      firstCard: null,
      secondCard: null,
    });
    this.setGameStatus('setSettings');
  };

  togglePause = (e) => {
    e.preventDefault();

    const { gameStatus } = this.state;

    if (gameStatus === 'running') {
      this.setGameStatus('pause');
      clearInterval(this.timer);
      this.setState({ firstCard: null, secondCard: null });
      this.availableCompare = false;
    } else if (gameStatus === 'pause') {
      this.startTimer();
      this.availableCompare = true;
    }
  };

  renderPopup = (status) => {
    let title, component;

    const renderButtons = (...buttons) => (
      <div className="popup__container">
        {buttons.map((button) => (
          <div key={button.text} className="popup__button" onClick={button.cb}>
            {button.text}
          </div>
        ))}
      </div>
    );

    const buttons = {
      backToMain: {
        text: 'Вернуться на главную',
        cb: () => this.props.history.push('/'),
      },
      restart: {
        text: 'Начать заново',
        cb: this.restartGame,
      },
      resume: {
        text: 'Продолжить игру',
        cb: this.togglePause,
      },
    };

    switch (status) {
      case 'setSettings':
        title = 'Выберите настройки игры';
        component = <Settings setProperties={this.setProperties} />;
        break;
      case 'win':
        title = 'Победа!';
        component = (
          <>
            <div className="popup__text">Поздравляем, Вы победили!!!</div>
            {renderButtons(buttons.backToMain, buttons.restart)}
          </>
        );
        break;
      case 'lose':
        title = 'Поражение.';
        component = (
          <>
            <div className="popup__text">
              К сожалению Вы не успели за отведенное время...
            </div>
            {renderButtons(buttons.backToMain, buttons.restart)}
          </>
        );
        break;
      case 'pause':
        title = 'Игра на паузе';
        component = <>{renderButtons(buttons.backToMain, buttons.resume)}</>;
        break;
      default:
        title = '';
        component = <h2>Произошла ошибка на сервере!</h2>;
    }

    return <Popup title={title}>{component}</Popup>;
  };

  render() {
    const { cards, checkedCards, firstCard, secondCard, gameStatus, timeLeft } =
      this.state;

    return (
      <>
        {gameStatus !== 'setSettings' && (
          <>
            <Header>
              <div className="header__title">Осталось времени: {timeLeft}</div>
              {gameStatus === 'running' && (
                <Nav>
                  <a href="/" onClick={this.togglePause}>
                    Пауза
                  </a>
                </Nav>
              )}
            </Header>

            <hr />

            <Cards
              cards={cards}
              checkedCards={checkedCards}
              firstCard={firstCard}
              secondCard={secondCard}
              onClick={this.compareCards}
            />
          </>
        )}

        {gameStatus !== 'running' && this.renderPopup(gameStatus)}
      </>
    );
  }
}
