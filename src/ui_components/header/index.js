import React from 'react';

// styles
import './styles.scss';

export default function Header({ children }) {
  return <div className="Header">{children}</div>;
}
