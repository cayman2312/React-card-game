import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

// styles
import './App.scss';

// components
import Main from './components/Main';
import Game from './containers/Game';

const App = () => (
  <div className="App">
    <div className="wrapper">
      <Switch>
        <Route path="/home" component={Main} />
        <Route path="/game" component={Game} />
        <Redirect from="/" to="/home" />
      </Switch>
    </div>
  </div>
);

export default App;
