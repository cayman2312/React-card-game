import React from 'react';

// styles
import './styles.scss';

export default function Nav({ children }) {
  return <div className="Nav">{children}</div>;
}
