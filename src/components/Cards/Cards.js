import React from 'react';
import './Cards.scss';
import Card from './Card/Card';

export default function Cards({
  cards,
  firstCard,
  secondCard,
  checkedCards,
  onClick,
}) {
  if (!cards.length) return null;

  return cards.map((card, index) => {
    const active = card.id === firstCard?.id || card.id === secondCard?.id;
    const checked = checkedCards.includes(card.id);

    return (
      <Card
        key={card.id}
        index={index}
        card={card}
        active={active}
        checked={checked}
        onClick={onClick}
      />
    );
  });
}
