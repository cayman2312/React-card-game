import React, { useEffect, useState } from 'react';
import cx from 'classnames';
import './Card.scss';

export default function Card({ card, index, onClick, checked, active }) {
  const [start, setStart] = useState(true);

  const cxCard = cx('Card', {
    Card__active: active,
    Card__checked: checked,
    Card__hidden: start,
  });

  useEffect(() => {
    setTimeout(() => {
      setStart(false);
    }, 30 * index);
  }, [index]);

  return (
    <div className="CardWrapper">
      <figure className={cxCard} onClick={() => onClick(card, active)}>
        <img
          className="back"
          src={require('../../../assets/images/cover.jpg')}
          alt="back"
          draggable="false"
        />
        <img className="front" src={card.img} alt="front" draggable="false" />
      </figure>
    </div>
  );
}
