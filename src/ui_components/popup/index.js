import React from 'react';

// styles
import './style.scss';

const Popup = ({ title, children }) => (
  <div className="Popup">
    <div className="Popup__window">
      {title && <h3 className="Popup__title">{title}</h3>}
      {children}
    </div>
  </div>
);

export default Popup;
