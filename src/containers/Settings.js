import React, { Component } from 'react';
import { Range, getTrackBackground } from 'react-range';

// styles
import './Settings.scss';

export default class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardAmount: [20],
      difficulty: [2],
      isRepeat: [1],
    };
  }

  onSubmit = (e) => {
    e.preventDefault();
    const initialState = { ...this.state };
    let { cardAmount, difficulty, isRepeat } = initialState;

    cardAmount = cardAmount[0];
    difficulty = difficulty[0];
    isRepeat = isRepeat[0] !== 1;

    this.props.setProperties({ cardAmount, difficulty, isRepeat });
  };

  render() {
    const { cardAmount, difficulty, isRepeat } = this.state;

    return (
      <form action="" className={'Form'} onSubmit={this.onSubmit}>
        <Range
          values={cardAmount}
          step={2}
          min={2}
          max={40}
          onChange={(cardAmount) => this.setState({ cardAmount })}
          renderTrack={({ props, children }) => (
            <div
              onMouseDown={props.onMouseDown}
              onTouchStart={props.onTouchStart}
              className="pointer"
              style={{
                ...props.style,
                height: '36px',
                display: 'flex',
                width: '100%',
              }}
            >
              <div
                ref={props.ref}
                style={{
                  height: '5px',
                  width: '100%',
                  borderRadius: '4px',
                  background: getTrackBackground({
                    values: cardAmount,
                    colors: ['#548BF4', '#ccc'],
                    min: 2,
                    max: 40,
                  }),
                  alignSelf: 'center',
                }}
              >
                {children}
              </div>
            </div>
          )}
          renderThumb={({ props, isDragged }) => (
            <div
              {...props}
              style={{
                ...props.style,
                height: '42px',
                width: '42px',
                borderRadius: '4px',
                backgroundColor: '#FFF',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                boxShadow: '0px 2px 6px #AAA',
              }}
            >
              <div
                style={{
                  height: '16px',
                  width: '5px',
                  backgroundColor: isDragged ? '#548BF4' : '#CCC',
                }}
              />
            </div>
          )}
        />
        <output className="Form__output" id="cardsAmount">
          Количество карточек:{' '}
          <span style={{ fontWeight: 'bold' }}>{cardAmount[0]}</span>
        </output>
        <br />
        <Range
          values={difficulty}
          step={1}
          min={1}
          max={3}
          onChange={(difficulty) => this.setState({ difficulty })}
          renderTrack={({ props, children }) => (
            <div
              onMouseDown={props.onMouseDown}
              onTouchStart={props.onTouchStart}
              className="pointer"
              style={{
                ...props.style,
                height: '36px',
                display: 'flex',
                width: '100%',
                justifyContent: 'center',
              }}
            >
              <div
                ref={props.ref}
                style={{
                  height: '5px',
                  width: '30%',
                  borderRadius: '4px',
                  background: getTrackBackground({
                    values: difficulty,
                    colors: ['#548BF4', '#ccc'],
                    min: 1,
                    max: 3,
                  }),
                  alignSelf: 'center',
                }}
              >
                {children}
              </div>
            </div>
          )}
          renderThumb={({ props, isDragged }) => (
            <div
              {...props}
              style={{
                ...props.style,
                height: '40px',
                width: '40px',
                borderRadius: '50%',
                backgroundColor: '#FFF',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                boxShadow: '0px 2px 6px #AAA',
                outline: 'none',
              }}
            >
              <div
                style={{
                  height: '15px',
                  width: '15px',
                  borderRadius: '50%',
                  backgroundColor: isDragged ? '#548BF4' : '#CCC',
                }}
              />
            </div>
          )}
        />
        <output className="Form__output" id="cardsAmount">
          Сложность:{' '}
          {difficulty[0] === 1 ? (
            <b>низкая {'😃'}</b>
          ) : difficulty[0] === 2 ? (
            <b>умеренная {'🙂'}</b>
          ) : (
            <b>высокая {'😤'}</b>
          )}
        </output>
        <br />
        <Range
          values={isRepeat}
          step={1}
          min={0}
          max={1}
          onChange={(isRepeat) => this.setState({ isRepeat })}
          renderTrack={({ props, children }) => (
            <div
              onMouseDown={props.onMouseDown}
              onTouchStart={props.onTouchStart}
              className="pointer"
              style={{
                ...props.style,
                height: '36px',
                display: 'flex',
                width: '100%',
                justifyContent: 'center',
              }}
            >
              <div
                ref={props.ref}
                style={{
                  height: '5px',
                  width: '10%',
                  borderRadius: '4px',
                  background: getTrackBackground({
                    values: isRepeat,
                    colors: ['#548BF4', '#ccc'],
                    min: 0,
                    max: 1,
                  }),
                  alignSelf: 'center',
                }}
              >
                {children}
              </div>
            </div>
          )}
          renderThumb={({ props, isDragged }) => (
            <div
              {...props}
              style={{
                ...props.style,
                height: '40px',
                width: '40px',
                borderRadius: '50%',
                backgroundColor: '#FFF',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                boxShadow: '0px 2px 6px #AAA',
                outline: 'none',
              }}
            >
              <div
                style={{
                  height: '15px',
                  width: '15px',
                  borderRadius: '50%',
                  backgroundColor: isDragged ? '#548BF4' : '#CCC',
                }}
              />
            </div>
          )}
        />
        <output className="Form__output" id="cardsAmount">
          Картинки
          {isRepeat[0] === 0 ? (
            <>
              <span style={{ fontWeight: 'bold' }}> могут </span>
              повторяться
            </>
          ) : (
            <>
              <span style={{ fontWeight: 'bold' }}> НЕ </span>
              повторяются
            </>
          )}
        </output>
        <br />
        <input type="submit" className="Form__submit" value="Старт!" />
      </form>
    );
  }
}
